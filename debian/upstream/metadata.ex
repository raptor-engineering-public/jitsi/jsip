# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/jain-sip-java/issues
# Bug-Submit: https://github.com/<user>/jain-sip-java/issues/new
# Changelog: https://github.com/<user>/jain-sip-java/blob/master/CHANGES
# Documentation: https://github.com/<user>/jain-sip-java/wiki
# Repository-Browse: https://github.com/<user>/jain-sip-java
# Repository: https://github.com/<user>/jain-sip-java.git
