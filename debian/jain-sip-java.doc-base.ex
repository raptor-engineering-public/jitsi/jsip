Document: jain-sip-java
Title: Debian jain-sip-java Manual
Author: <insert document author here>
Abstract: This manual describes what jain-sip-java is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/jain-sip-java/jain-sip-java.sgml.gz

Format: postscript
Files: /usr/share/doc/jain-sip-java/jain-sip-java.ps.gz

Format: text
Files: /usr/share/doc/jain-sip-java/jain-sip-java.text.gz

Format: HTML
Index: /usr/share/doc/jain-sip-java/html/index.html
Files: /usr/share/doc/jain-sip-java/html/*.html
